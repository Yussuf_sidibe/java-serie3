package org.YussufSidibe.Serie3.exo8;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map.Entry;

public class RegistreMarin {
	//Champs ou attributs
	private Hashtable<Integer,HashSet<Marin>> table;
	// Constructeur
	public RegistreMarin(){
		table=new Hashtable<>();
	}
	
	@Override
	public String toString() {
		return "RegistreMarin [table=" + table + "]";
	}

	//M�thodes
	public void addMarin(Marin m){
		int key=m.getAnneeNaissance()%100;
		key=key-(key%10);
		HashSet<Marin> listMarin=new HashSet<>();
		if(this.table.isEmpty() || !this.table.isEmpty() && !table.containsKey(key) )
		{
			listMarin.clear();
			listMarin.add(m);
			this.table.put(key,listMarin);
		}
		else
			this.table.get(key).add(m);
	}
	public HashSet<Marin> get(int decennie){
		return this.table.get(decennie);
	}
	public int count(int decennie){
		return this.table.get(decennie).size();
	}
	public int count(){
		
		int nombreMarins=0;
		for (Entry<Integer, HashSet<Marin>> entree : this.table.entrySet()) {
			nombreMarins+=count(entree.getKey());
		}
		return nombreMarins;
	}
}
