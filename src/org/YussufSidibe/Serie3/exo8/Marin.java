package org.YussufSidibe.Serie3.exo8;

public class Marin {
	private String prenom;
	private String nom;
	private int anneeNaissance;
	
	public Marin(){}
	public Marin(String prenom, String nom,int anneeNais){
		this.setPrenom(prenom);
		this.setNom(nom);
		this.setAnneeNaissance(anneeNais);
	}
	@Override
	public String toString() {
		return "Marin [prenom=" + prenom + ", nom=" + nom + ", anneeNaissance=" + anneeNaissance + "]";
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAnneeNaissance() {
		return anneeNaissance;
	}
	public void setAnneeNaissance(int anneeNaissance) {
		this.anneeNaissance = anneeNaissance;
	}
}