package org.YussufSidibe.Serie3.exo7;



import java.util.Collection;
import java.util.HashSet;


public class Equipage {

	//Attributs ou champs
	private Collection <Marin> collect;
	private int nbreMax;
	
	// Constructeurs
	public Equipage(){
		this.collect=new HashSet<Marin>();
	}
	public Equipage(int nbre){
		this.collect=new HashSet<Marin>();
		this.nbreMax = nbre; 
	}
	
	// Getters et Setters
	public Collection<Marin> getCollect() {
		return collect;
	}
	public void setCollect(Collection<Marin> collect) {
		this.collect = collect;
	}
	public int getNbreMax() {
		return nbreMax;
	}
	public void setNbreMax(int nbreMax) {
		this.nbreMax = nbreMax;
	}
	
	
	// Les m�thodes de la classe Equipage
public boolean addMarin(Marin m)	// cette m�thode permet d'ajouter un marin dans un �quipage 
{
		return this.collect.add(m);
	}
	public boolean addMarin2(Marin m) // cette m�thode permet de d'ajouter un marin dans un �quipage si le nombre maximal de marins dans l'�quipage n'est pas atteint
	{
		boolean b=false;
		if(collect.size()<this.nbreMax)
			b=this.collect.add(m);
		return b;
	}
	public boolean removeMarin(Marin m) // Cette m�thode permet de retirer un marin dans un �quipage
	{
		
		return this.collect.remove(m);	
	}
	public boolean isMarinPresent(Marin m)// V�rifie si un marin est pr�sent dans un �quipage
	{
		return this.collect.contains(m);
	}
	public void addAllEquipage(Equipage e)// copie le contenu d'un �quipage dans l'�quipage courant.
	{
		for(Marin c: e.getCollect())
			this.addMarin(c);
	}
	public void clear()// Cette m�thode efface le contenu de l'�quipage 
	{
		this.collect.clear();
	}
	public int getNombreMarins(){
		
		return this.collect.size();
	}
	public int getMoyenneSalaire(){
		int som=0;
		for(Marin c: getCollect())
			som+=c.getSalaire();
		return som/this.getNombreMarins();
	}
	@Override
	public String toString() {
		return "Equipage [collect=" + collect + ", nbreMax=" + nbreMax + "]";
	}
}