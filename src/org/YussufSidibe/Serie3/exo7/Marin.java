package org.YussufSidibe.Serie3.exo7;



public class Marin implements Comparable<Marin> {
	
	private String nom;
	private String prenom;
	private int salaire;
	
	public Marin(String nom,String prenom,int salaire) // constructeur qui permet d'initialiser le nom le prenom et le salaire du marin
	{
		this.nom=nom;
		this.prenom=prenom;
		this.salaire=salaire;
	}
	public Marin(String name,int salaire) // consructeur qui permet d'initialiser le nom et le salaire du marin
	{
		this(name,"",salaire);
	}
	//getters&setters
	public int getSalaire() {
		return salaire;
	}
	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	//fin getters&setters
	
	public void augmenteSalaire(int augmentation) // Cette methode permet d'augmenter le salaire d'un marin d'une valeur donnee en argument
	{
		this.salaire+=augmentation;
	}
	@Override
	public String toString() {
		return "Marin [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + salaire;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (salaire != other.salaire)
			return false;
		return true;
	}
	@Override
	public int compareTo(Marin m) {
		// TODO  Auto-generated method stub
		if(getNom().equals(m.getNom()))
			return (this.prenom.compareTo(m.getPrenom()));
		else
			return (this.nom.compareTo(m.getNom()));
	}
	
}
