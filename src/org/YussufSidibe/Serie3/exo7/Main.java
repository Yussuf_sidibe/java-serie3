package org.YussufSidibe.Serie3.exo7;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Equipage equip,equip1,equip2;;
		equip=new Equipage();
		Marin m1,m2,m3,m4,m5,m6 ;
		// test des m�thodes addMarin(), removeMarin(), isMarinPresent() et toString()
		equip.addMarin(m1=new Marin ("DIALLO",100));
		equip.addMarin(m2=new Marin ("BARRY",200));
		equip.addMarin(m3=new Marin ("SOW",300));
		equip.addMarin(m4=new Marin ("SY",400));
		equip.addMarin(m5=new Marin ("Ly",500));
		equip.addMarin(m6=new Marin ("Sarr",500));
		System.out.println(equip);
		equip.removeMarin(m5);
		System.out.println(equip);
		System.out.println("Le marin m1 est dans l'�quipage :"+equip.isMarinPresent(m1));
		System.out.println("Le marin m5 est pas dans l'�quipage: "+equip.isMarinPresent(m5));
		
		//Test de la m�thode addAllEquipage()
		equip1=new Equipage();
		equip1.addMarin(m1);
		equip1.addMarin(m2);
		equip1.addMarin(m3);
		equip1.addMarin(m5);
		equip1.addMarin(m6=new Marin ("Diaw",500));
		equip.addAllEquipage(equip1);
		System.out.println(equip);
		
		// Test de la m�thode clear()
		equip.clear();;
		System.out.println(equip);
		
		//Test de la m�thode getNombreMarins() et getMoyenneSalaire()
		System.out.println(equip1.getNombreMarins());
		System.out.println(equip1.getMoyenneSalaire());
		
		//Test de la m�thode adMarin2
		equip2=new Equipage(3);
		equip2.addMarin2(m1);
		equip2.addMarin2(m2);
		equip2.addMarin2(m3);
		equip2.addMarin2(m4);
		System.out.println(equip2);
	}

}
